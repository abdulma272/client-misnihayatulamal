import React from "react";
import { Plane } from "../../assets/img/icon";

const missionStatements = [
  "Menumbuhkan kebiasaan membaca, menulis, dan menghapal Al-Qur’an sejak dini.",
  "Membentuk anak Kreatif, berani, berprestasi dan berakhlak mulia.",
  "Mewujudkan generasi tangguh yang siap menghadapi tuntutan zaman.",
];

const Misi = () => {
  return (
    <div className="w-full bg-white font-andika">
      <div className=" sm:pl-20 pl-8">
        <div className="flex">
          <img
            src={Plane}
            alt="Plane"
            className="sm:w-[40.026px] sm:h-[40.026px] w-[30px] h-[30px]"
          />
          <p className="pl-2 text-[20px] sm:text-[24px] font-bold">MISI</p>
        </div>
        <ul className="list-disc mx-9 font-w-400 sm:leading-[40px] leading-[24px] sm:w-100 text-[17px] text-justify sm:text-left sm:text-[20px] ">
          {missionStatements.map((statement, index) => (
            <li key={index}>{statement}</li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Misi;
