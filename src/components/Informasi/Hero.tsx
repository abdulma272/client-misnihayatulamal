import Sliders from "./Sliders";
import { Beranda } from "../Interface/Beranda.interface";
import { useFetchData } from "../hooks/fetchData";
import { handlerApi } from "../service/handlerApi";
import { Informasi } from "../Interface/Informasi.interface";

const Hero = () => {
  const { data } = useFetchData<Informasi>(() =>
    handlerApi(
      "/api/informasi?populate[0]=Informasi&populate[1]=Slide.Image&populate[2]=Persyaratan_Umur&populate[3]=Persyaratan_Administrasi"
    )
  );

  // Menambahkan penanganan jika data tidak tersedia
  if (!data) {
    return (
      <div className="flex justify-center items-center h-screen">
        <div className="animate-spin rounded-full h-10 w-10 border-t-2 border-b-2 border-gray-900"></div>
        <p>Loading.....</p>
      </div>
    );

    return <div>Loading...</div>;
  }

  return (
    <div className="max-w-full">
      <div className="bg-[#954141] fixed sm:mt-[80px] mt-[60px] z-20 w-full p-1 text-[12px] sm:text-[18px] text-white">
        {/* Adding marquee effect to the title */}
        {data.attributes.Informasi.map((teks, index) => (
          <p className="marquee text-full">{teks.Deskripsi}</p>
        ))}
      </div>

      <Sliders />
    </div>
  );
};

export default Hero;
