import { ImageType, MetaPage } from "./StrapiType";

export interface KurikulumPageResponse {
  id: number;
  attributes: {
    Judul: string;
    Image: ImageType;
    Deskripsi: [
      {
        Paragraf: string;
      }
    ];
  };
}
[];
