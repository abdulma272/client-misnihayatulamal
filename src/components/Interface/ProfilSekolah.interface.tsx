import { ImageType } from "./StrapiType";

export interface ProfilSekolah {
  id: Number;
  attributes: {
    Visi_Sekolah: string;
    Dashboard_Sekolah: Dashboards;
    PageKurikulum: PageKurikulums;
    programs: program;
    Misi_Sekolah: Misis;
  };
}

type Dashboards = {
  id: Number;
  Judul: string;
  Deskripsi: [
    {
      Paragraf: string;
    }
  ];
  Image: ImageType;
};

type PageKurikulums = {
  id: Number;
  Deskripsi: [
    {
      Paragraf: string;
    }
  ];
  kurikulums: {
    data: [
      {
        id: number;
        attributes: {
          Judul: String;
          Image: ImageType;
          Deskripsi: [{ Paragraf: string }];
        };
      }
    ];
  };
};
type program = {
  data: [
    {
      id: Number;
      attributes: {
        Judul: string;
        Deskripsi: [{ id: number; Paragraf: string }];
        slug: string;
        Slide: [
          {
            Judul: string;
            Image: ImageType;
          }
        ];
      };
    }
  ];
};

type Misis = {
  id: Number;
  Paragraf: string;
}[];
