type ImageType = {
  data: {
    id: number;
    attributes: {
      url: string;
    };
  };
};

type MetaPage = {
  pagination: {
    page: number;
    pageSize: number;
    pageCount: number;
    total: number;
  };
};

type SEOType = {
  pageTitle: string;
  pageMeta: string;
  pageDescription: string;
  pageBanner: ImageType;
};

export type { ImageType, MetaPage, SEOType };
