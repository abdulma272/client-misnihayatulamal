import { ImageType } from "./StrapiType";

export interface KontaksResponses {
  data: Kontaks;
  meta: {};
}

export interface Kontaks {
  id: Number;
  attributes: {
    Dashboard_Kontak: {
      id: number;
      Judul: string;
      Deskripsi: string;
      Maps: string;
      Image: ImageType;
    };
    Kontak: contact[];
    Sosials: contact[];
  };
}

type contact = {
  id: Number;
  Judul: string;
  Deskripsi: string;
  Url: string;
  Image: ImageType;
};
