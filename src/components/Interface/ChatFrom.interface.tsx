export interface ChatFromResponse {
  data: ChatFrom[];
  meta: {};
}

export interface ChatFrom {
  data: {
    Nama_lengkap: string;
    Email: string;
    No_Telepon: string;
    Pesan: string;
  };
}
