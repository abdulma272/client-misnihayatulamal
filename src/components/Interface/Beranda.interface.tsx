import { Attributes } from "react";
import { ImageType } from "./StrapiType";

export interface BerandaResponse {
  data: Beranda;
  meta: {};
}

export interface Beranda {
  id: Number;
  attributes: {
    Layanan: [{ Judul: string; Deskripsi: String; Image: ImageType }];
    artikels: {
      data: [
        {
          id: Number;
          attributes: {
            Judul: String;
            Slug: string;
            Deskripsi_Artikel: [
              {
                id: number;
                Paragraf: string;
              }
            ];
            UpdateArtikel: string;
            Image: ImageType;
          };
        }
      ];
    };
    Teks: String;
    paqs: {
      Pertanyaan: string;
      Jawaban: string;
    }[];
  };
}
