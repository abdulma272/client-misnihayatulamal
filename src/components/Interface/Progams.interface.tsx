import { ImageType } from "./StrapiType";

export interface Programs {
  id: Number;
  attributes: {
    Judul: string;
    Deskripsi: [
      {
        id: number;
        Paragraf: string;
      }
    ];
    slug: string;
    Slide: [
      {
        Judul: string;
        Image: ImageType;
      }
    ];
  };
}
