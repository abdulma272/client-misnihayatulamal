import { ImageType } from "./StrapiType";

export interface Slide {
  id: Number;
  attributes: {
    Image: ImageType;
  };
}
