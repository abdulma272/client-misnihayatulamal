import { ImageType } from "./StrapiType";

export interface InformasiResponse {
  data: Informasi;
  meta: {};
}

export interface Informasi {
  id: number;
  attributes: {
    Informasi: [
      {
        id: number;
        Deskripsi: string;
      }
    ];
    Slide: {
      id: number;
      judul: string;
      Image: ImageType;
    }[];
    Persyaratan_Umur: [
      {
        id: Number;
        Paragraf: String;
      }
    ];

    Persyaratan_Administrasi: [
      {
        id: Number;
        Paragraf: String;
      }
    ];
  };
}
