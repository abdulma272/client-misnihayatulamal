import { ImageType, MetaPage } from "./StrapiType";

export interface ArtikelResponse {
  data: Artikel[];
  meta: MetaPage;
}

export interface Artikel {
  id: Number;
  attributes: {
    Judul: string;
    Slug: string;
    UpdateArtikel: string;
    Image: ImageType;
    Deskripsi_Artikel: [
      {
        id: number;
        Paragraf: string;
      }
    ];
  };
}
