import { ImageType } from "./StrapiType";

export interface PondokResponse {
  data: Pondok;
  meta: {};
}

export interface Pondok {
  id: string;
  attributes: {
    Dashboard_Pondok: Dashboards[];
    Visi_Pondok: string;
    Misi_Pondok: [{ id: number; Paragraf: string }];
    Kategori_Asrama: [
      {
        id: number;
        Judul: string;
        Deskripsi: [
          {
            Paragraf: string;
          }
        ];
        Image: ImageType;
      }
    ];
  };
}

type Dashboards = {
  id: Number;
  Judul: string;
  Deskripsi: {
    id: number;
    Paragraf: string;
  }[];
  Image: ImageType;
};
