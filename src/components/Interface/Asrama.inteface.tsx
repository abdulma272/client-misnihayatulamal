import { ImageType } from "./StrapiType";

export interface AsramasResponse {
  data: Asrama[];
  meta: {};
}

export interface Asrama {
  id: Number;
  attributes: {
    Judul: string;
    Deskripsi: [
      {
        Paragraf: string;
      }
    ];
    Slug: string;
    Kategori: string;
    Image: ImageType;
    Slide: [
      {
        id: number;
        Judul: string;
        Image: ImageType;
      }
    ];
    urlWhatsApp: string;
    urlPendaftaran: ImageType;
  };
}
