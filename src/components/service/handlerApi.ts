//HandleApi.tsx
import axios from "axios";

const Bearer_token = process.env.REACT_APP_API_TOKEN;

export const HTTPTarwil = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
  headers: {
    Authorization: `Bearer ${Bearer_token}`,
  },
});

export const handlerApi = async (endpoint: string) => {
  try {
    const res = await HTTPTarwil.get(endpoint);
    return res.data.data;
  } catch (error) {
    console.error(error);
  }
};
